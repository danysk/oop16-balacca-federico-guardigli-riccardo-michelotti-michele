package controller;

import java.util.List;

import model.Garment;
import utilities.Brand;
import utilities.DressSize;
import utilities.SearchImpl;
import utilities.TypeDress;
import view.filterbar.FilterBarController;

public class SearchControllerImpl implements SearchController {
	
		private List<Garment> itemViewList;
		private FilterBarController fbc = FilterBarController.getIstance();
		
		private static SearchControllerImpl istance=null; 

		
		
	    private SearchControllerImpl() {}

	    
	    public static SearchControllerImpl getIstance() {
	    	
	        	synchronized (SearchControllerImpl.class) {
	        			
	        			if(istance==null){
	        				
	        					istance = new SearchControllerImpl();
	        			
	        			}
	        	}
	        	
	        	return istance;
	        	
	    }
	    
	
	    public List<Garment> search(List<Garment> list, TypeDress dress, Brand brand, DressSize size, boolean onSale, double priceMin, double priceMax ) {
		
		
	    		this.itemViewList = list;
		
	    		if(dress != TypeDress.ANY){
			
	    				this.itemViewList = SearchImpl.searchByType(itemViewList, dress);
			
	    		}
		
	    		if(fbc.isAdvfilterson()){
			
			
	    				this.itemViewList = SearchImpl.searchByPrice(itemViewList, priceMin, priceMax);
		
	    				if( brand != Brand.ANY){
			
	    						this.itemViewList = SearchImpl.searchByBrand(itemViewList, brand);
			
	    				}
		
	    				if ( size != DressSize.ANY){
			
	    						this.itemViewList = SearchImpl.searchBySize(itemViewList, size);
			
	    				}
		
	    				if (onSale == true){
			
	    						this.itemViewList = SearchImpl.searchByDiscount(itemViewList);
	    						
	    				}	
	    		}
		
	    		return this.itemViewList;
	    }
}
