package view.speech;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public final class SpeechController {

    @FXML
    private TextArea txt_speech;
    
    @FXML
    private BorderPane bp_root;
    
    private static SpeechController istance=null;
    
    private SpeechController() {}

    public static SpeechController getIstance() {
        synchronized (SpeechController.class) {
            if(istance==null){
                    istance = new SpeechController();
            }
        }
        return istance;
    }

    public void setOutput(String output){
    	txt_speech.appendText(output);
    }
    
    public void clearOutput(){
    	txt_speech.clear();
    }
    
    public void close(){
    	Stage stage=(Stage) txt_speech.getScene().getWindow();
    	stage.close();
    	//txt_speech.getScene().getWindow().hide();
    }
}
