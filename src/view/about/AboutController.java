package view.about;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

public final class AboutController {
	
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private VBox vbox_menu;
    
    private static AboutController istance=null;
    
    private AboutController() {}

    public static AboutController getIstance() {
        synchronized (AboutController.class) {
            if(istance==null){
                    istance = new AboutController();
            }
        }
        return istance;
    }
    
    @FXML
    void initialize() {
    	view.utils.Utils.loadSidebar(vbox_menu);

    }

}
