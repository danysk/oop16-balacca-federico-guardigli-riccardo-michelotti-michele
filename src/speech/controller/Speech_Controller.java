package speech.controller;


import speech.model.SpeechModel;


public interface Speech_Controller {

	//it calls the model and enables the recognition
	/**
	 * it calls the start of the voice
	 */
	void enableVoiceRecognition();

	/**
	 * it sets the model
	 * @param model
	 */
	void setModel(SpeechModel model);
}


