package model;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.*;

public class MailSender {
	
	/**
	 * this generic class is used to send a mail to a specific recipient using smtp protocol
	 */
	
	/**
	 * this method send a mail when a product is in stock again
	 * @param recipients
	 * @param subject
	 * @param message
	 * @param from
	 * @param username
	 * @param password
	 * @throws MessagingException
	 */
	 
	public void sendMail(String recipients[ ], String subject, String message , String from, String username, String password) throws MessagingException
	{
	  boolean debug = false;
	  
	  /**
	   * Creating session, authenticate and setting properties
	   */
	 
	  Properties props = new Properties();
	  props.put("mail.smtp.starttls.enable", true);
	  props.put("mail.smtp.host", "smtp.gmail.com");
	  props.put("mail.smtp.user", username);
	  props.put("mail.smtp.password", password);
	  props.put("mail.smtp.port", "587");
	  props.put("mail.smtp.auth", true);
	  

	  Session session = Session.getDefaultInstance(props,
	          new Authenticator() {
                protected PasswordAuthentication  getPasswordAuthentication() {
                    return new PasswordAuthentication(
                                username+"@gmail.com", password);
                }});
	  session.setDebug(debug);
	  
	  /*
	   * Create the message
	   */
	 
	  Message msg = new MimeMessage(session);
	 
	  /*
	   * InternetAdress class built an object describing an e-mail adress
	   */
	  
	  InternetAddress addressFrom = new InternetAddress(from);
	  msg.setFrom(addressFrom);
	 
	  InternetAddress[] addressTo = new InternetAddress[recipients.length]; 
	  for (int i = 0; i < recipients.length; i++)
	  {
	    addressTo[i] = new InternetAddress(recipients[i]);
	  }
	  
	  /*
	   * Setting message recipients
	   */
	  
	  msg.setRecipients(Message.RecipientType.TO, addressTo);
	 
	  /*
	   * Setting message subject
	   */
	  msg.setSubject(subject);
	  
	  /*
	   * Setting message content
	   */
	  msg.setContent(message, "text/plain");
	  
	  /*
	   * Send method in Transport class effectively send the message
	   */
	  
	  Transport.send(msg);
	}

}
