package model;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.Serializable;

import utilities.Brand;
import utilities.DressSize;
import utilities.TypeDress;

public class Test implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fileName1 = "testShelf.ser";
	private String fileName2 = "testGarment.ser";
	private IdCodeCounter id = new IdCodeCounterImpl();

			
		@org.junit.Test
	    public void testBasicShelfs() throws IOException, ClassNotFoundException {
	        Shop shop = ShopImpl.getIstance();
	        shop.addNewShelf(2, 2);
	        SerializationUtil.serialize(shop, fileName1);
	        Shop shop2 = (Shop) SerializationUtil.deserialize(fileName1);
	        assertEquals(shop2.getListofShelfs().get(0).getPosXShelf(),2);
	        assertEquals(shop2.getListofShelfs().get(0).getPosYShelf(),2);
	        shop2.addNewShelf(3, 3);
	        shop2.modifyShelf(0, 4, 4, 1);
	        SerializationUtil.serialize(shop2, fileName1);
	        Shop shop3 = (Shop) SerializationUtil.deserialize(fileName1);
	        assertEquals(shop3.getListofShelfs().size(), 2);
	        assertEquals(shop3.getListofShelfs().get(0).getPosXShelf(),4);
	    }
		
		@org.junit.Test
	    public void testBasicGarment() throws IOException, ClassNotFoundException {
	        Shop shop = ShopImpl.getIstance();
	        shop.insertNewGarment("Air Max", 200.00, Brand.NIKE, false, 1, TypeDress.SHOES, null, DressSize.L, id.getCode());
	        shop.insertNewGarment("Run", 100.00, Brand.NIKE, true, 2, TypeDress.SHOES, null, DressSize.L, id.getCode());
	        SerializationUtil.serialize(shop, fileName2);
	        Shop shop2 = (Shop) SerializationUtil.deserialize(fileName2);
	        assertEquals(shop2.getListofGarment().get(0).getCodeNum(),1);
	        assertEquals(shop2.getListofGarment().get(0).getBrand(),Brand.NIKE);
	        assertEquals(shop2.getListofGarment().get(1).getTypeDress(),TypeDress.SHOES);
	    }
		
}
