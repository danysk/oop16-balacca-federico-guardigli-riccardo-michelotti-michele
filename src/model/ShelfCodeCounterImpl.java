package model;

import java.io.Serializable;

public class ShelfCodeCounterImpl implements ShelfCodeCounter, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int codeNum;
	
    public ShelfCodeCounterImpl() {
		// TODO Auto-generated constructor stub
    		this.codeNum=0;
	}

	@Override
	public int getCode() {	
		// TODO Auto-generated constructor stub
		nextCode();
		return this.codeNum;		
	}
	
	public void nextCode() {
		// TODO Auto-generated constructor stub 
		this.codeNum++;
	}

}
