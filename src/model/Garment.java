package model;

import utilities.Brand;
import utilities.DressSize;
import utilities.TypeDress;

public interface Garment {
	
	/** 
	 * 	 * these methods are used to get every attribute from the field
	 * */
	String getName();
	double getPrice();
	Brand getBrand();
	Boolean getDiscount(); //getSconto
	int getShelfNum();
	TypeDress getTypeDress();
	String getPhoto();
	DressSize getSize();
	long getCodeNum();
	
	/**
	 * this method is used to set, if any, the discount of the field
	 * @param b
	 */
	void setDiscount(Boolean b);
	/**
	 * this method is used to set the price of the field
	 * @param fPrice
	 */
	void setPrice(Double fPrice);
	/**
	 * this method is used to set the number of shelf in which the garment will be exposed
	 * @param nShelf
	 */
	void setShelfNum(Integer nShelf);
	/**
	 * this method is used to set the path of a picture for the garment
	 * @param path
	 */
	void setPhoto(String path);
	/**
	 * this method is used to set the name of the field
	 * @param name
	 */
	void setName(String name);
	/**
	 * this method is used to set the brand of the field (selected from an enum)
	 * @param brand
	 */
	void setBrand(Brand brand);
	/**
	 * this method is used to set the size of the field (selected from an enum)
	 * @param size
	 */
	void setSize(DressSize size);
	/**
	 * this method is used to set the type of the field (selected from an enum)
	 * @param dress
	 */
	void setType(TypeDress dress);

}
