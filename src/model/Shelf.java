package model;

public interface Shelf {
	
	/**
	 * this method is used to get the progressive number of a shelf
	 * @return shelf progressive code
	 */
	int getNShelf();
	/**
	 * this method is used to get the X position of a shelf
	 * @return posX
	 */
	int getPosXShelf();
	/**
	 * this method is used to get the Y position of a shelf
	 * @return posY
	 */
	int getPosYShelf();
	/**
	 * this method is used to set the progressive number of a shelf
	 * @param n
	 */
	void setNShelf(int n);
	/**
	 *  this method is used to set the X, Y position of a shelf
	 * @param nX
	 * @param nY
	 */
	void setPosShelf(int nX, int nY);
	
}
