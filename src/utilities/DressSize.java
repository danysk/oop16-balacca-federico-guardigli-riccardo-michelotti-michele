package utilities;

import java.util.HashMap;
import java.util.Map;

public enum DressSize {
	S("small"), M("medium"), L("large"), XL("extra large"), XXL("double extra large"), ANY(" ");
	
	private String word;
	
	DressSize(String word){
		
		this.word = word;
		
	}

	
	public String word(){
		
		return this.word;
		
	}
	
	private static Map<String, DressSize> map = new HashMap<>();
	
	static {
		for(DressSize ds : DressSize.values()){
			
			map.put(ds.word, ds);
			
		}
		
	}
	
	public static DressSize getEnum(String word){
		
		return map.get(word);

	}
	
}
